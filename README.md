El repositorio contiene el ejercicio final del 2º trimestre de la asignatura acceso a datos del IES portada alta cuaderno de clase.

La aplicación ofrece a los profesores la posibilidad de realizar el seguimiento académico de sus alumnos. Cuenta con 3 ventanas:
	-un menú inicial con las opciones alumno y seguimiento
	-una lista de alumnos que permite realizar operaciones CRUD 
	-una lista con los alumnos y su actividad académica del dia

La aplicación se comunica con una API REST contruida sobre el framework slim, documentación de la API: https://andres.alumno.club/

El código de la API y las sentencias de la base de datos se encuentran en la raíz del proyecto con los nombres de slim.zip y cuaderno.sql.

Se requieren permisos de internet en el dispositivo para la utilización de la aplicación.
Para la comunicación HTTP se ha utilizado android asynchronous HTTP client.

Dependencias necesarias:
	compile 'com.android.support:design:25.1.1'
compile group: 'cz.msebera.android' , name: 'httpclient', version: '4.4.1.1'
compile 'com.loopj.android:android-async-http:1.4.9'
compile 'com.google.code.gson:gson:2.8.0'