package com.example.espino.cuaderno;

import java.io.Serializable;


public class Alumno implements Serializable{
    private int id;
    private String apellidos, nombre, direccion, ciudad, cp, telefono, email;

    public Alumno(int id, String apellidos, String nombre, String direccion, String ciudad, String cp, String telefono, String email) {
        this.id = id;
        this.apellidos = apellidos;
        this.nombre = nombre;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.cp = cp;
        this.telefono = telefono;
        this.email = email;
    }

    public Alumno(String apellidos, String nombre, String direccion, String ciudad, String cp, String telefono, String email) {
        this.apellidos = apellidos;
        this.nombre = nombre;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.cp = cp;
        this.telefono = telefono;
        this.email = email;
    }


    public Alumno() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
