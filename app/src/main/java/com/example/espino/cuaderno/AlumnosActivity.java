package com.example.espino.cuaderno;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;


public class AlumnosActivity extends AppCompatActivity {

    public static final String URL_API_ALL = "alumnos";
    public static final String URL_API = "alumno";
    public static final String EMAIL = "email";

    public static final int ADD_CODE = 100;
    public static final int UPDATE_CODE = 200;
    public static final int OK = 1;

    private ListView list;
    private AlumnosAdapter adapter;
    private FloatingActionButton btn;

    private int positionClicked;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumnos);

        list = (ListView) findViewById(android.R.id.list);
        btn = (FloatingActionButton) findViewById(R.id.alumnos_btn);

        adapter = new AlumnosAdapter(this);
        list.setAdapter(adapter);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AlumnosActivity.this, NuevoAlumnoActivity.class);
                startActivityForResult(i, ADD_CODE);
            }
        });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                showPopup(view,i);
                return true;
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent emailIntent = new Intent(getApplicationContext(), EmailActivity.class);
                emailIntent.putExtra(EMAIL, adapter.getItem(i).getEmail());
                startActivity(emailIntent);
            }
        });

        downloadAlumnos();

    }

    private void downloadAlumnos() {
        final ProgressDialog progreso = new ProgressDialog(this);
        RestClient.get(URL_API_ALL, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Connecting . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        adapter.set(result.getAlumnos());
                        message = "Connection OK";
                    } else
                        message = result.getMessage();
                else
                    message = "Null data";
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Error: " + throwable.getCause() + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Error: " + throwable.getCause() + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Alumno al = null;
        if (requestCode == ADD_CODE)
            if (resultCode == OK) {
                al = (Alumno) data.getSerializableExtra("alumno");
                adapter.add(al);
            }
          if (requestCode == UPDATE_CODE)
            if (resultCode == OK) {
                al = (Alumno) data.getSerializableExtra("alumno");
                adapter.modifyAt(positionClicked, al);
            }
    }

    private void showPopup(View v, final int position) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.getMenuInflater().inflate(R.menu.context_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.contextmenu_modificar:
                        modify(adapter.getItem(position));
                        positionClicked = position;
                        return true;
                    case R.id.contextmenu_eliminar:
                        confirm(adapter.getItem(position));
                        positionClicked = position;
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }

    private void modify(Alumno al) {
        Intent i = new Intent(this, ModificarAlumnoActivity.class);
        i.putExtra("alumno", al);
        startActivityForResult(i, UPDATE_CODE);
    }



    private void confirm(final Alumno al) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Quieres eliminar al alumno " + al.getApellidos() + "?")
                .setTitle("Eliminar")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        connection(al.getId(), positionClicked);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    private void connection(int id, final int position) {
        final ProgressDialog progreso = new ProgressDialog(this);
        RestClient.delete(URL_API + "/" + id, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Connecting . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        message = "Alumno borrado OK";
                        adapter.removeAt(position);
                    } else
                        message = "Error deleting:\nEstado: " + result.getStatus() + "\n" +
                                result.getMessage();
                else
                    message = "Null data";
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
