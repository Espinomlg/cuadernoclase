package com.example.espino.cuaderno;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class AlumnosAdapter extends ArrayAdapter<Alumno>{

    public AlumnosAdapter(Context context) {
        super(context, R.layout.listitem_alumno, new ArrayList<Alumno>());
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AlumnoHolder holder = null;

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_alumno, parent, false);
            holder = new AlumnoHolder();

            holder.apellidos = (TextView) convertView.findViewById(R.id.alumnoitem_apellidos);
            holder.nombre = (TextView) convertView.findViewById(R.id.alumnoitem_nombre);

            convertView.setTag(holder);
        }
        else
            holder = (AlumnoHolder) convertView.getTag();

          holder.apellidos.setText(getItem(position).getApellidos());
          holder.nombre.setText(getItem(position).getNombre());

        return convertView;
    }

    public void set(ArrayList<Alumno> lista){
        addAll(lista);
    }

    public void modifyAt(int position, Alumno al){
        Alumno old = getItem(position);
        remove(old);
        add(al);
    }
    public void removeAt(int position){
        Alumno old = getItem(position);
        remove(old);
    }

    public static class AlumnoHolder{
        private TextView apellidos, nombre;
    }
}
