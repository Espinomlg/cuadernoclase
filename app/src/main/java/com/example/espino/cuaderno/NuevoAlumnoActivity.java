package com.example.espino.cuaderno;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;



public class NuevoAlumnoActivity extends AppCompatActivity {

    public static final String URL = "alumno";
    public static final int OK = 1;

    private TextInputLayout apellidos, nombre, direccion, ciudad, cp , telefono, email;
    private Button guardar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manejaralumno);

        apellidos = (TextInputLayout) findViewById(R.id.manalumno_apellidos);
        nombre = (TextInputLayout) findViewById(R.id.manalumno_nombre);
        direccion = (TextInputLayout) findViewById(R.id.manalumno_direccion);
        ciudad = (TextInputLayout) findViewById(R.id.manalumno_ciudad);
        cp = (TextInputLayout) findViewById(R.id.manalumno_codpostal);
        telefono = (TextInputLayout) findViewById(R.id.manalumno_telefono);
        email = (TextInputLayout) findViewById(R.id.manalumno_email);

        guardar = (Button) findViewById(R.id.manalumno_guardar);
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkNotEmpty()){
                    Alumno al = new Alumno(apellidos.getEditText().getText().toString(),
                            nombre.getEditText().getText().toString(),
                            direccion.getEditText().getText().toString(),
                            ciudad.getEditText().getText().toString(),
                            cp.getEditText().getText().toString(),
                            telefono.getEditText().getText().toString(),
                            email.getEditText().getText().toString());

                    connection(al);
                }
            }
        });
    }

    private void connection(final Alumno al) {
        final ProgressDialog progreso = new ProgressDialog(this);
        RequestParams params = new RequestParams();
        //apellidos, nombre, dirección, ciudad, código postal, teléfono y email.
        params.put("apellidos", al.getApellidos());
        params.put("nombre", al.getNombre());
        params.put("direccion", al.getDireccion());
        params.put("ciudad", al.getCiudad());
        params.put("cp", al.getCp());
        params.put("telefono", al.getTelefono());
        params.put("email", al.getEmail());
        RestClient.post(URL, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Connecting . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                //Site site;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        message = "alumno aladido ok";
                        Intent i = new Intent();
                        Bundle mBundle = new Bundle();
                        al.setId(result.getLast());
                        mBundle.putSerializable("alumno", al);
                        i.putExtras(mBundle);
                        setResult(OK, i);
                        finish();
                    } else
                        message = "Error añadiendo al alumno:\n" + result.getMessage();
                else
                    message = "Null data";
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean checkNotEmpty(){
        boolean ok = true;

        apellidos.setError("");
        nombre.setError("");
        direccion.setError("");
        ciudad.setError("");
        cp.setError("");
        telefono.setError("");
        email.setError("");
        String error = "El campo no puede estar vacío";

        if(TextUtils.isEmpty(apellidos.getEditText().getText().toString())){
            apellidos.setError(error);
            apellidos.requestFocus();
            ok = false;
        }
        else if(TextUtils.isEmpty(direccion.getEditText().getText().toString())){
            direccion.setError(error);
            direccion.requestFocus();
            ok = false;
        }
        else if(TextUtils.isEmpty(nombre.getEditText().getText().toString())){
            nombre.setError(error);
            nombre.requestFocus();
            ok = false;
        }
        else if(TextUtils.isEmpty(ciudad.getEditText().getText().toString())){
            ciudad.setError(error);
            ciudad.requestFocus();
            ok = false;
        }
        else if(TextUtils.isEmpty(cp.getEditText().getText().toString())){
            cp.setError(error);
            cp.requestFocus();
            ok = false;
        }
        else if(TextUtils.isEmpty(telefono.getEditText().getText().toString())){
            telefono.setError(error);
            telefono.requestFocus();
            ok = false;
        }
        else if(TextUtils.isEmpty(email.getEditText().getText().toString())){
            email.setError(error);
            email.requestFocus();
            ok = false;
        }

        return ok;
    }
}
