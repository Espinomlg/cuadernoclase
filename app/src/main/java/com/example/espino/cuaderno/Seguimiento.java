package com.example.espino.cuaderno;


public class Seguimiento {

    private int id;
    private String apellidos, nombre, falta, trabajo, actitud, observaciones;

    public Seguimiento(int id, String apellidos, String nombre, String falta, String trabajo, String actitud, String observaciones) {
        this.id = id;
        this.apellidos = apellidos;
        this.nombre = nombre;
        this.falta = falta;
        this.trabajo = trabajo;
        this.actitud = actitud;
        this.observaciones = observaciones;
    }

    public Seguimiento(String apellidos, String nombre, String falta, String trabajo, String actitud, String observaciones) {
        this.apellidos = apellidos;
        this.nombre = nombre;
        this.falta = falta;
        this.trabajo = trabajo;
        this.actitud = actitud;
        this.observaciones = observaciones;
    }

    public Seguimiento() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFalta() {
        return falta;
    }

    public void setFalta(String falta) {
        this.falta = falta;
    }

    public String getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(String trabajo) {
        this.trabajo = trabajo;
    }

    public String getActitud() {
        return actitud;
    }

    public void setActitud(String actitud) {
        this.actitud = actitud;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Seguimiento{" +
                "id=" + id +
                ", apellidos='" + apellidos + '\'' +
                ", nombre='" + nombre + '\'' +
                ", falta='" + falta + '\'' +
                ", trabajo='" + trabajo + '\'' +
                ", actitud='" + actitud + '\'' +
                ", observaciones='" + observaciones + '\'' +
                '}';
    }
}
