package com.example.espino.cuaderno;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.util.TextUtils;


public class SeguimientoActivity extends AppCompatActivity {

    private static final String URL_API = "seguimiento";

    private ListView lista;
    private FloatingActionButton btn;
    private SeguimientoAdapter adapter;
    private ProgressDialog progreso;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seguimiento);

        lista = (ListView) findViewById(android.R.id.list);
        btn = (FloatingActionButton) findViewById(R.id.seguimiento_btn);
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);

        adapter = new SeguimientoAdapter(this);
        lista.setAdapter(adapter);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progreso.show();
                saveData();
            }
        });

        downloadSeguimiento();
    }

    private void downloadSeguimiento() {
        final ProgressDialog progreso = new ProgressDialog(this);
        RestClient.get(URL_API, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Connecting . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        adapter.set(result.getSeguimiento());
                        message = "Connection OK";
                    } else
                        message = result.getMessage();
                else
                    message = "Null data";
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Error: " + throwable.getCause() + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Error: " + throwable.getCause() + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void connection(final Seguimiento seg, final boolean ultimo) {
        RequestParams params = new RequestParams();
        params.put("falta", seg.getFalta());
        params.put("trabajo", seg.getTrabajo());
        params.put("actitud", seg.getActitud());
        params.put("observaciones", seg.getObservaciones());

        RestClient.put(URL_API + "/" + seg.getId(), params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (ultimo)
                    progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null) {
                    if (ultimo && result.getCode()) {
                        message = "Seguimiento actualizado ok";
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }
                    else if(!result.getCode() && ultimo){
                        message = "No ha habido cambios";
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }
                }
                else if(result == null && ultimo){
                        message = "Ha habido un error";
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void saveData(){
        String falta = "",
                trabajo = "",
                actitud = "",
                observaciones = "";

        boolean last = false;

        for(int i = 0; i < adapter.getCount(); i++){
            //$dbquery = $this->db->prepare("UPDATE " . TABLE_SEGUIMIENTO_ALUMNOS . " SET falta = ?, trabajo = ?, actitud = ?, observaciones = ? WHERE id = ?");
            LinearLayout item = (LinearLayout) lista.getChildAt(i);

            for(int index = 0; index < item.getChildCount(); index++){
                EditText text = null;
                switch (item.getChildAt(index).getId()){
                    case R.id.segitem_falta:
                        text = (EditText) item.getChildAt(index);
                        falta = TextUtils.isEmpty(text.getText().toString()) ? "" : text.getText().toString();
                        break;
                    case R.id.segitem_trabajo:
                        text = (EditText) item.getChildAt(index);
                        trabajo = TextUtils.isEmpty(text.getText().toString()) ? "" : text.getText().toString();
                        break;
                    case R.id.segitem_actitud:
                        text = (EditText) item.getChildAt(index);
                        actitud = TextUtils.isEmpty(text.getText().toString()) ? "" : text.getText().toString();
                        break;
                    case R.id.segitem_observaciones:
                        text = (EditText) item.getChildAt(index);
                        observaciones = TextUtils.isEmpty(text.getText().toString()) ? "" : text.getText().toString();
                        break;
                }
            }
//int id, String apellidos, String nombre, String falta, String trabajo, String actitud, String observaciones
            Seguimiento seg = new Seguimiento(adapter.getItem(i).getId(),
                    adapter.getItem(i).getApellidos(),
                    adapter.getItem(i).getNombre(),
                    falta,
                    trabajo,
                    actitud,
                    observaciones);

            if(i == adapter.getCount() - 1)
                last = true;

            connection(seg, last);
        }
    }

}

