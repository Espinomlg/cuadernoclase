package com.example.espino.cuaderno;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class SeguimientoAdapter extends ArrayAdapter<Seguimiento>{

    public SeguimientoAdapter(Context context) {
        super(context, R.layout.listitem_seguimiento, new ArrayList<Seguimiento>());
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SeguimientoHolder holder = null;

        if(convertView == null){
            holder = new SeguimientoHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_seguimiento, parent, false);

            holder.alumno = (TextView) convertView.findViewById(R.id.segitem_alumno);
            holder.falta = (EditText) convertView.findViewById(R.id.segitem_falta);
            holder.trabajo = (EditText) convertView.findViewById(R.id.segitem_trabajo);
            holder.actitud = (EditText) convertView.findViewById(R.id.segitem_actitud);
            holder.observaciones = (EditText) convertView.findViewById(R.id.segitem_observaciones);

            convertView.setTag(holder);
        }
        else
            holder = (SeguimientoHolder) convertView.getTag();

        holder.alumno.setText(getItem(position).getApellidos() + ", " + getItem(position).getNombre());
        holder.falta.setText(getItem(position).getFalta() != null ? getItem(position).getFalta() : "");
        holder.trabajo.setText(getItem(position).getTrabajo() != null ? getItem(position).getTrabajo() : "");
        holder.actitud.setText(getItem(position).getActitud() != null ? getItem(position).getActitud() : "");
        holder.observaciones.setText(getItem(position).getObservaciones() != null ? getItem(position).getObservaciones() : "");

        return convertView;
    }

    public void set(ArrayList<Seguimiento> lista){
        addAll(lista);
    }

    public static class SeguimientoHolder {

        private TextView alumno;
        private EditText falta, trabajo, actitud, observaciones;
    }

}
