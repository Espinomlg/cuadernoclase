-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 08-03-2017 a las 21:30:00
-- Versión del servidor: 5.7.17-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cuadernoDBandres`
--
CREATE DATABASE IF NOT EXISTS `cuadernoDBandres` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cuadernoDBandres`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `id` int(11) NOT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `cp` char(5) DEFAULT NULL,
  `telefono` char(9) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`id`, `apellidos`, `nombre`, `direccion`, `ciudad`, `cp`, `telefono`, `email`) VALUES
(1, 'Espino Lopez', 'Andres', 'c/inventada', 'Malaga', '29631', '618428471', 'espinomlg@gmail.com'),
(4, 'Fernandez Baca', 'Eduardo', 'c/ imagilandia', 'Malaga', '29018', '958741236', 'edufz@gmail.com'),
(6, 'Badillo Rojas', 'Javier', 'c/ inexistente', 'malaga', '24532', '625841239', 'badillo@hotmail.com');

--
-- Disparadores `alumno`
--
DELIMITER $$
CREATE TRIGGER `trigger_delete_alumno` AFTER DELETE ON `alumno` FOR EACH ROW BEGIN
	DELETE FROM seguimiento_alumno WHERE seguimiento_alumno.alumno = OLD.id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trigger_insert_alumno` AFTER INSERT ON `alumno` FOR EACH ROW BEGIN
	INSERT INTO seguimiento_alumno (alumno, fecha) VALUES (NEW.id, CURRENT_DATE);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguimiento_alumno`
--

CREATE TABLE `seguimiento_alumno` (
  `id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `alumno` int(11) DEFAULT NULL,
  `falta` char(1) DEFAULT NULL,
  `trabajo` char(1) DEFAULT NULL,
  `actitud` char(1) DEFAULT NULL,
  `observaciones` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `seguimiento_alumno`
--

INSERT INTO `seguimiento_alumno` (`id`, `fecha`, `alumno`, `falta`, `trabajo`, `actitud`, `observaciones`) VALUES
(1, '2017-03-08', 1, 'i', 'b', 'p', 'buenas'),
(2, '2017-03-08', 4, 'j', 'r', 'n', 'ninguna'),
(5, '2017-03-08', 6, 'i', 'm', '', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seguimiento_alumno`
--
ALTER TABLE `seguimiento_alumno`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumno`
--
ALTER TABLE `alumno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `seguimiento_alumno`
--
ALTER TABLE `seguimiento_alumno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
DELIMITER $$
--
-- Eventos
--
CREATE DEFINER=`alumno`@`%` EVENT `evento_seguimiento_alumno` ON SCHEDULE EVERY 1 DAY STARTS '2017-03-02 00:00:00' ON COMPLETION PRESERVE ENABLE DO INSERT INTO seguimiento_alumno (alumno, fecha) SELECT id, CURRENT_DATE FROM alumno$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
